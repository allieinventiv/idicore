#!/bin/bash

# duplicate data folder, add site to sites.php, duplicate sitefolder, ignore sass cache folder, remove sass cache

CUR_USER=`whoami`
KZ_DIR=/opt/dev/kazaamprojects
ALPHA_SITE_DIR=`pwd`
ALPHA_SITE_DATA=$TEMPLATE_DIR/data
ALPHA_SITE_WEB=$TEMPLATE_DIR/web


#Update template-site first from git
#git pull

# create site and folder structure
echo "Enter production domain with extension (example: mysite.com)"
read FULL_DOMAIN

# change domain string to be all lowercase
FULL_DOMAIN=`echo $FULL_DOMAIN | tr '[:upper:]' '[:lower:]'`
echo $FULL_DOMAIN

# everything from the left without the dot
SERVER_NAME=${FULL_DOMAIN%.*}
#remove characters for database variables
SITE_NAME=${SERVER_NAME//[-.]/\_}

# Top Level Domain - everything to the right with a leading dot
TLD=.${FULL_DOMAIN#*.}

# MS = multisite
MS_SITE_DIR=ALPHA_SITE_DIR/
MS_DIR_DATA=ALPHA_SITE_DIR/data/$SITE_NAME
MS_DIR_WEB=ALPHA_SITE_DIR/web/$SITE_NAME

#------change this ----
SITE_DIR=$KZ_DIR/$SITE_NAME
SITE_DATA_DIR=$SITE_DIR/data/$SITE_NAME
SITE_WEB_DIR=$SITE_DIR/web

if [ -d $SITE_DIR ]
then
    echo "$SITE_DIR already exists. If you mean create it again please delete $SITE_DIR folder and run this script again. Terminating."
    exit
fi
# -----------------------------


#generate a random password
function gen_password() {
local LENGTH=$1  #how long = length of password
local CASES=$2   #how strong
local CHARACTERS="A-Za-z0-9_\!\@\#\$\%\^\&\*\-+"

case "$CASES" in
    1)
   local CHARACTERS="A-Za-z0-9";;
esac #end case

if [ "`which uname /dev/null 2>&1`" ]; then
  case "`uname -a`" in
    Darwin*)
    local DB_PASS=`LC_CTYPE=C tr -dc $CHARACTERS < /dev/urandom | head -c $LENGTH | xargs` ;;
    *)
    local DB_PASS=`tr -dc $CHARACTERS < /dev/urandom | head -c $LENGTH | xargs` ;;
  esac
fi

echo $DB_PASS
}

#remove special characters and truncate site name for the database
DB_VAR=`echo ${SITE_NAME:0:15}`

#${SITE_NAME:0:15 -dc $CHARACTERS}`

DB_USER=$DB_VAR
echo "Your database user was generated for you $DB_USER"

DB_NAME="$DB_USER"_site
echo "Your database name was generated for you $DB_NAME"

#Decide if user is running a Unix shell or OSX
DB_PASS=$(gen_password 20 1)
echo "Your secure database password was generated for you \"$DB_PASS\""

#make data directory
mkdir -p $SITE_DATA_DIR
rsync -a --delete $TEMPLATE_DIR_DATA/ $SITE_DATA_DIR/

echo "Creating your database for you. This will take a few seconds..."
#Find and replace all instances of templatesite" with $sitename
#http://stackoverflow.com/questions/6178498/using-grep-and-sed-to-find-and-replace-a-string
#http://stackoverflow.com/questions/7573368/in-place-edits-with-sed-on-os-x
find $SITE_DATA_DIR -type f -exec sed -i "" "s/SITE_NAME/$SITE_NAME/g" {} \;
find $SITE_DATA_DIR -type f -exec sed -i "" "s/SERVER_NAME/$SERVER_NAME/g" {} \;
find $SITE_DATA_DIR -type f -exec sed -i "" "s/FULL_DOMAIN/$FULL_DOMAIN/g" {} \;
find $SITE_DATA_DIR -type f -exec sed -i "" "s/DB_USER/$DB_USER/g" {} \;
find $SITE_DATA_DIR -type f -exec sed -i "" "s/DB_PASS/$DB_PASS/g" {} \;
find $SITE_DATA_DIR -type f -exec sed -i "" "s/DB_NAME/$DB_NAME/g" {} \;


#remove forward slash to allow sed to work. Will add the forward slashes back in
TEMP_SITE_DIR="${SITE_WEB_DIR//\//slash}"
find $SITE_DATA_DIR -type f -exec sed -i "" "s/SITE_WEB_DIR/$TEMP_SITE_DIR/g" {} \;

#replace the $$ tokens back to forward slash to restore path
#http://forums.devshed.com/unix-help-35/sed-escaping-forward-slash-393115.html
find $SITE_DATA_DIR -type f -exec sed -i "" "s#slash#\/#g" {} \;

#make web folder
mkdir -p $SITE_WEB_DIR

#duplicate the core profile to make multisite setup
rsync -a --delete $TEMPLATE_DIR_WEB/ $SITE_WEB_DIR/
rsync -a --delete  $SITE_WEB_DIR/sites/idicore/ $SITE_WEB_DIR/sites/$SITE_NAME
#rename variables to the new site name theme folder | .info file and .install file
mv  $SITE_WEB_DIR/sites/$SITE_NAME/themes/idicore $SITE_WEB_DIR/sites/$SITE_NAME/themes/$SITE_NAME
mv $SITE_WEB_DIR/sites/$SITE_NAME/themes/$SITE_NAME/idicore.info $SITE_WEB_DIR/sites/$SITE_NAME/themes/$SITE_NAME/$SITE_NAME.info
find $SITE_WEB_DIR/sites/$SITE_NAME/themes/$SITE_NAME/$SITE_NAME.info -type f -exec sed -i "" "s/SITE_NAME/$SITE_NAME/g" {} \;
find $SITE_WEB_DIR/profiles/idicore/idicore.install -type f -exec sed -i "" "s/SITE_NAME/$SITE_NAME/g" {} \;
find $SITE_WEB_DIR/sites/$SITE_NAME/themes/$SITE_NAME/template.php -type f -exec sed -i "" "s/idicore/$SITE_NAME/g" {} \;
find $SITE_WEB_DIR/sites/$SITE_NAME/themes/$SITE_NAME/preprocess/* -type f -exec sed -i "" "s/idicore/$SITE_NAME/g" {} \;
find $SITE_WEB_DIR/sites/$SITE_NAME/themes/$SITE_NAME/process/* -type f -exec sed -i "" "s/idicore/$SITE_NAME/g" {} \;


cd $SITE_WEB_DIR/sites/$SITE_NAME/
chmod u+x settings.php

find $SITE_WEB_DIR/sites/$SITE_NAME/settings.php -type f -exec sed -i "" "s/DB_USER/$DB_USER/g" {} \;
find $SITE_WEB_DIR/sites/$SITE_NAME/settings.php -type f -exec sed -i "" "s/DB_PASS/$DB_PASS/g" {} \;
find $SITE_WEB_DIR/sites/$SITE_NAME/settings.php -type f -exec sed -i "" "s/DB_NAME/$DB_NAME/g" {} \;
find $SITE_WEB_DIR/sites/sites.php -type f -exec sed -i "" "s/SITE_NAME/$SITE_NAME/g" {} \;
find $SITE_WEB_DIR/sites/sites.php -type f -exec sed -i "" "s/SERVER_NAME/$SERVER_NAME/g" {} \;
find $SITE_WEB_DIR/sites/sites.php -type f -exec sed -i "" "s/FULL_DOMAIN/$FULL_DOMAIN/g" {} \;


# create user
echo "CREATE USER '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS';" | mysql -u root
echo "GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS' WITH GRANT OPTION;" | mysql -u root
echo "GRANT ALL PRIVILEGES ON \`$DB_USER\_%\`.* TO '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS' WITH GRANT OPTION;" | mysql -u root
echo "GRANT FILE ON *.* TO '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS';" | mysql -u root

# create database
echo "SET FOREIGN_KEY_CHECKS=0;DROP DATABASE IF EXISTS $DB_NAME;CREATE DATABASE $DB_NAME DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;SET FOREIGN_KEY_CHECKS=1;" | mysql -u root

cd $SITE_WEB_DIR
drush si idicore -y --db-url=mysql://$DB_USER:$DB_PASS@localhost/$DB_NAME --db-su=root --account-name=admin --account-pass=kzFbc975@ --site-name=$SITE_NAME --account-mail=ajones@inventivdigital.com --sites-subdir=$SITE_NAME

cd $SITE_DATA_DIR
chmod u+x setup-local-site
./setup-local-site true


open http://$SERVER_NAME.dev
