# Project Overview

* FP Job:
* Git Branching: Git Flow
* Staging Server: STWEB2
* Production Server:
* Framework:
* Database: Drupal 7
* UI/ Responsive Framework: Susy 2
* Build Tools: Grunt, npm
* Drupal Config: Site install profile Multisite

This is a multisite setup.
modules for all sites are located in sites/all/modules
Anything specific to each site will be located in sites/SITENAME/themes/SITENAME

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet


# To Do list
- ~~Create homepage node~~
- ~~panalizer~~
- ~~beans~~
- ~~pathauto variables~~
- ~~set up default panels instead of regions~~
- ~~default beans~~
- Notice: Undefined property: stdClass::$dir in omega_css_alter() (line 276 of /opt/dev/kazaamprojects/icttemplate/web/sites/all/themes/omega/omega/template.php).
- ~~faq question content type~~
- pull all content out into a separate module
- fix sass variables
- dist folder and grunt
- create users and set permissions
- svgezy
- theme settings variables
- check all install uninstall enable and disable of custom modules.
- theme settings and variables 
- setup-local-site script add front end configs
- create multisite script
- remove overlay


#pages with theme name variables
- all preprocess pages
- themesettings.php
- .install
- .info


# To Research
-insert field module
-field collection ajax



# Other NOTES
drush si idicore -y --db-url=mysql://$DB_USER:$DB_PASS@localhost/$DB_NAME --db-su=root --account-name=admin --account-pass=kzFbc975@ --site-name=$SITE_NAME --account-mail=ajones@inventivdigital.com --sites-subdir=$SITE_NAME

drush si idicore -y --db-url=mysql://idicore:9i1nHMMJjfa9Na8qWNof@localhost/idicore_site --db-su=root --account-name=admin --account-pass=kzFbc975@ --site-name=idicore --account-mail=ajones@inventivdigital.com --sites-subdir=idicore


$form['#submit'][] = 'aveenomd_export_users_settings_form_submit';
$node_wrapped = entity_metadata_wrapper(‘node’, $node);
$node_wrapped->save;

/// install and disable all module put at top of module
function aveenomd_commerce_install(){
    //Enable the included features module that contains some field/content type configuration
    if (!module_exists('aveeno_commerce_config')) {
      $modules = array('aveeno_commerce_config');
      module_enable($modules);
    }
    aveenomd_commerce_create_products();
}

function aveenomd_commerce_uninstall(){
    //Disable the included features module that contains some field/content type configuration
    if (module_exists('aveeno_commerce_config')) {
      $modules = array('aveeno_commerce_config');
      module_disable($modules);
    }
}

///


# multisite config

- to download modules for the alpha site alpha-sitename/web 
- to download modules for the beta site alpha-sitename/web/sites/beta-site 