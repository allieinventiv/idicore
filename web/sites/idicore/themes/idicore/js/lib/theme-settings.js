(function ($) {
// Color picker

    var cpClass = 'colorSelect2';
    var colorpickerHTML = '<span class="colorSelect"><span></span></span>';

    // add input for colorselect input field
    $('input#edit-menu-color').before('<div class="color-pick-field"><div id="colorSelector"></div></div>');
    $('input#edit-text-link-color').before('<span id="text-link-color-select"' +  cpClass + '"><span></span></span>');
    $('input#edit-text-link-hover-color').before('<span id="text-link-hover-color-select"' +  cpClass + '"><span></span></span>');
    $('input#edit-button-color').before('<span id="button-color-select"' +  cpClass + '"><span></span></span>');
    $('input#edit-button-hover-color').before('<span id="button-hover-color"' +  cpClass + '"><span></span></span>');



 // Color picker
/*
    $(document).ready(function() {
        $('#colorPick').ColorPicker({
            color: '#0000ff',
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }
        })
    });

*/

})(jQuery);