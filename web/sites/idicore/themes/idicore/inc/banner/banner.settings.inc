<?php

/**
 * @file
 * Contains the theme settings form elements for the layouts extension.
 */

/**
 * Implements hook_extension_EXTENSION_theme_settings_form_alter().
 */
function icttemplate_extension_banner_settings_form($element, &$form, $form_state) {

  // banner settings
//  if ((!$key) || in_array('banner', $features)) {
    $form['banner'] = array(
      '#type' => 'fieldset',
      '#title' => t('banner image settings'),
      '#description' => t('If toggled on, the following banner will be displayed.'),
      '#attributes' => array('class' => array('theme-settings-bottom')),
    );
    $form['banner']['default_banner'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the default banner'),
      '#default_value' => theme_get_setting('default_banner'),
      '#tree' => FALSE,
      '#description' => t('Check here if you want the theme to use the banner supplied with it.')
    );
    $form['banner']['settings'] = array(
      '#type' => 'container',
      '#states' => array(
        // Hide the banner settings when using the default banner.
        'invisible' => array(
          'input[name="default_banner"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['banner']['settings']['banner_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to custom banner'),
      '#description' => t('The path to the file you would like to use as your banner file instead of the default banner.'),
      '#default_value' => t('banners/'),
    );

    $form['banner']['settings']['banner_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload banner image'),
      '#maxlength' => 40,
      '#description' => t("Use this field to upload your banner.")
    );
//  }


  return $form['banner'];

  // Inject human-friendly values for logo and favicon.
  foreach (array('logo' => 'logo.png') as $type => $default) {
    if (isset($form[$type]['settings'][$type . '_path'])) {
      $element = &$form[$type]['settings'][$type . '_path'];

      // If path is a public:// URI, display the path relative to the files
      // directory; stream wrappers are not end-user friendly.
      $original_path = $element['#default_value'];
      $friendly_path = NULL;
      if (file_uri_scheme($original_path) == 'public') {
        $friendly_path = file_uri_target($original_path);
        $element['#default_value'] = $friendly_path;
      }
    }
  }

  $form['banner_test'] = array(
   '#type'          => 'textfield',
   '#title'         => t('Widget'),
   '#default_value' => theme_get_setting('banner_test'),
   '#description'   => t("Place this text in the widget spot on your site."),
 );

 $form['foo_example'] = array(
  '#type'          => 'textfield',
  '#title'         => t('Widget'),
  '#default_value' => theme_get_setting('foo_example'),
  '#description'   => t("Place this text in the widget spot on your site."),
);

}
