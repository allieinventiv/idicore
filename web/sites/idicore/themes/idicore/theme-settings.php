<?php


function idicore_form_system_theme_settings_alter(&$form, &$form_state) {
    global $base_url;
  // Add a checkbox to toggle the breadcrumb trail.
  //https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7
 ///dpm($form);
    // https://www.drupal.org/node/2186031
// http://ghosty.co.uk/2014/03/managed-file-upload-in-drupal-theme-settings/

    $form['general_elements'] = array(
        '#type' => 'fieldset',
        '#title' => t('general elements'),
        '#weight' => 1,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['general_elements']['banner_image'] = array(
        '#title' => t('Banner Image'),
        '#description' => t('Image should be less than 400 pixels wide and in JPG format.'),
        '#type' => 'managed_file',
        '#upload_location' => 'public://banners/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
        '#default_value' => theme_get_setting('banner_image'),
    );
    $form['#submit'][] = 'idicore_settings_form_submit';
// Get all themes.
    $themes = list_themes();
// Get the current theme
    $active_theme = $GLOBALS['theme_key'];
    $form_state['build_info']['files'][] = str_replace("/$active_theme.info", '', $themes[$active_theme]->filename) . '/theme-settings.php';

//--------------------- colors -----------------------------------------------//

    drupal_add_js(libraries_get_path('bgrins-spectrum') . '/spectrum.js');
    drupal_add_css(libraries_get_path('bgrins-spectrum') . '/spectrum.css');
    $spectrum_js = 'jQuery(".spectrum-color-picker").spectrum({
    showInput: true,
    allowEmpty: true,
    showAlpha: true,
    showInitial: true,
    showInput: true,
    preferredFormat: "hex",
    clickoutFiresChange: true,
    showButtons: false
  });';

    drupal_add_js($spectrum_js, array('type' => 'inline', 'scope' => 'footer'));

    $form['colors'] = array(
        '#type' => 'fieldset',
        '#title' => t('colors'),
        '#weight' => 1,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
    );

    $form['colors']['settings']['header_bg_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Header Background Color'),
        '#default_value' => theme_get_setting('header_bg_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

     $form['colors']['settings']['menu_bg_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Menu Background Color'),
        '#default_value' => theme_get_setting('menu_bg_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
         '#attributes' => array('class' => array('spectrum-color-picker')),
     );

    $form['colors']['settings']['menu_link_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Menu Text Links Color'),
        '#default_value' => theme_get_setting('menu_link_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

    $form['colors']['settings']['menu_link_hover_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Menu text Links Hover Color'),
        '#default_value' => theme_get_setting('menu_link_hover_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

    $form['colors']['settings']['menu_link_bg_hover_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Menu Text Links Hover Color'),
        '#default_value' => theme_get_setting('menu_link_bg_hover_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

    $form['colors']['settings']['body_bg_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Main Body Background Color'),
        '#default_value' => theme_get_setting('body_bg_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );
    $form['colors']['settings']['footer_bg_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Footer Background Color'),
        '#default_value' => theme_get_setting('footer_bg_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

    $form['colors']['settings']['button_bg_color'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Button Background Color'),
        '#default_value' => theme_get_setting('button_bg_color', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

    $form['colors']['settings']['text_link'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Text Link Color'),
        '#default_value' => theme_get_setting('text_link', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

    $form['colors']['settings']['text_link_hover'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Text Link Hover Color'),
        '#default_value' => theme_get_setting('text_link_hover', 'idicore'),
        '#description'   => t("Place this text in the widget spot on your site."),
        '#attributes' => array('class' => array('spectrum-color-picker')),
    );

    $form['colors']['empty_colors'] = array(
        '#type' => 'submit',
        '#value' => t('Empty Colors'),
        '#weight' => 15,
        '#submit' => array('idicore_empty_colors_form_submit'),
    );

    $form['colors']['default_colors'] = array(
        '#type' => 'submit',
        '#value' => t('IDI Colors'),
        '#weight' => 15,
        '#submit' => array('idicore_default_colors_form_submit'),
    );
   $form['#submit'][]= 'idicore_values_form_submit';

//dsm($form);
}

function idicore_settings_form_submit(&$form, $form_state) {
    $image_fid = $form_state['values']['banner_image'];
    $image = file_load($image_fid);
    if (is_object($image)) {
        // Check to make sure that the file is set to be permanent.
        if ($image->status == 0) {
            // Update the status.
            $image->status = FILE_STATUS_PERMANENT;
            // Save the update.
            file_save($image);
            // Add a reference to prevent warnings.
            file_usage_add($image, 'idicore', 'theme', 1);
        }
    }
}


function idicore_default_colors_form_submit(&$form, $form_state)
{
    dsm('default colors');
    //dsm($form_state['values']['colors']['settings']);
    $theme = 'idicore';
    // Get the settings
    $idicore_settings = variable_get('theme_idicore_settings', array());
   // dsm($idicore_settings);
    $defaults = array(
         'header_bg_color' => '#959699',
        'menu_bg_color' => '#1a3d73',
        'menu_link_color' => '#ffffff',
        'button_bg_color' => '#1a3d73',
        'menu_link_bg_hover_color' => '#25c0f1',
        'menu_link_hover_color' => '#1a3d73',
        'body_bg_color' => '#ededed',
        'footer_bg_color' => '#CCCCCC',
        'text_link' => '#1a3d73',
        'text_link_hover' => '#25c0f1',
    );
    foreach($form_state['values']['colors']['settings'] as $variable => $value){
        $idicore_settings[$variable] = $defaults[$variable];

    }

// Save our settings
    variable_set('theme_idicore_settings', $idicore_settings);
}

function idicore_empty_colors_form_submit(&$form, $form_state) {

      dsm('empty colors');
    //dsm($form_state['values']['colors']['settings']);
    $theme = 'idicore';
    // Get the settings
    $idicore_settings = variable_get('theme_idicore_settings', array());
    // dsm($idicore_settings);
    $defaults = array(
        'header_bg_color' => '',
        'menu_bg_color' => '',
        'menu_link_color' => '',
        'button_bg_color' => '',
        'menu_link_bg_hover_color' => '',
        'menu_link_hover_color' => '',
        'body_bg_color' => '',
        'footer_bg_color' => '',
        'text_link' => '',
        'text_link_hover' => '',
    );
    foreach($form_state['values']['colors']['settings'] as $variable => $value){
        $idicore_settings[$variable] = $defaults[$variable];
    }
    variable_set('theme_idicore_settings', $idicore_settings);

}

function idicore_values_form_submit(&$form, $form_state){
     dsm('form submit');
    dsm($form_state['values']['colors']['settings']);
     $idicore_settings = variable_get('theme_idicore_settings', array());
    foreach ($form_state['values']['colors']['settings'] as $key => $value) {
        $idicore_settings[$key] = $value;
       // dsm($key);   dsm($value);
    }

     variable_set('theme_idicore_settings', $idicore_settings);
      /*  if (is_array($value) && isset($form_state['values']['colors']['setttings'])) {
            $value = array_keys(array_filter($value));
        }
        variable_set($key, $value);     */
}